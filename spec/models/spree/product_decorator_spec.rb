require "rails_helper"

RSpec.describe Spree::Product, type: :model do
  describe "scope" do
    describe "#related_products" do
      subject { described_class.related_products(main_product) }

      let(:taxonomy1) { create(:taxonomy) }
      let(:taxonomy2) { create(:taxonomy) }
      let(:main_product) { create(:base_product, taxons: [taxonomy1.root]) }
      let(:related_product) { create(:base_product, taxons: [taxonomy1.root]) }
      let(:unrelated_product) { create(:base_product, taxons: [taxonomy2.root]) }

      it "同じtaxonに属する商品を関連商品として返すこと" do
        is_expected.to contain_exactly(related_product)
      end

      it "異なるtaxonに属する商品を関連商品として返さないこと" do
        is_expected.not_to include(unrelated_product)
      end

      it "戻り値に引数の商品自身を含めて返さないこと" do
        is_expected.not_to include(main_product)
      end
    end

    describe "#new_products" do
      subject { described_class.new_products }

      let!(:new_products) do
        [
          create(:base_product, available_on: 1.month.ago),
          create(:base_product, available_on: 2.months.ago),
          create(:base_product, available_on: 3.months.ago),
          create(:base_product, available_on: 4.months.ago),
          create(:base_product, available_on: 5.months.ago),
          create(:base_product, available_on: 6.months.ago),
          create(:base_product, available_on: 7.months.ago),
          create(:base_product, available_on: 8.months.ago)
        ]
      end

      it "新着商品を降順で取得できる" do
        is_expected.to match_array(new_products)
      end
    end
  end
end
