# frozen_string_literal: true

require "rails_helper"

feature "#show Potepan::Products" do
  given(:taxonomy) { create(:taxonomy) }
  given(:product) { create(:base_product, name: "product", price: 123.00, taxons: [taxonomy.root]) }
  given!(:related_product1) { create(:base_product, name: "related_product1", price: 111.00, taxons: [taxonomy.root]) }
  given!(:related_product2) { create(:base_product, name: "related_product2", price: 222.00, taxons: [taxonomy.root]) }
  given!(:related_product3) { create(:base_product, name: "related_product3", price: 333.00, taxons: [taxonomy.root]) }
  given!(:related_product4) { create(:base_product, name: "related_product4", price: 444.00, taxons: [taxonomy.root]) }
  given!(:product_property) { create(:product_property, product: product) }

  background do
    visit potepan_product_path(product)
  end

  scenario "割り当てた商品の情報が正しく表示されている" do
    expect(page).to have_http_status(200)
    expect(current_path).to eq potepan_product_path(product)
    expect(page).to have_title("product")

    within(".singleProduct") do
      expect(page).to have_content("product")
      expect(page).to have_content(product.description)
      expect(page).to have_content("$123.00")
      expect(page).to have_content(product_property.value)
    end
  end

  scenario "商品の関連商品が正しく表示されている" do
    expect(page).to have_http_status(200)
    expect(current_path).to eq potepan_product_path(product)

    within(".productsContent") do
      expect(page).to have_content("related_product1")
      expect(page).to have_content("$111.00")
      expect(page).to have_content("related_product2")
      expect(page).to have_content("$222.00")
      expect(page).to have_content("related_product3")
      expect(page).to have_content("$333.00")
      expect(page).to have_content("related_product4")
      expect(page).to have_content("$444.00")
    end
  end

  scenario "関連商品のリンクから正しく遷移できる" do
    within(".productsContent") do
      click_link("related_product1")
    end

    expect(page).to have_http_status(200)
    expect(current_path).to eq potepan_product_path(related_product1)
    expect(page).to have_title("related_product1")

    within(".singleProduct") do
      expect(page).to have_content("related_product1")
      expect(page).to have_content(related_product1.description)
      expect(page).to have_content("$111.00")
    end

    within(".productsContent") do
      expect(page).to have_content("product")
      expect(page).to have_content("$123.00")
      expect(page).to have_content("related_product2")
      expect(page).to have_content("$222.00")
      expect(page).to have_content("related_product3")
      expect(page).to have_content("$333.00")
      expect(page).to have_content("related_product4")
      expect(page).to have_content("$444.00")
    end
  end
end
