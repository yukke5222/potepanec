# frozen_string_literal: true

require "rails_helper"

feature "#show Potepan::Categories" do
  given(:taxonomy) { create(:taxonomy) }
  given(:child_taxon) { create(:taxon, name: "child taxon", taxonomy: taxonomy, parent: taxonomy.root) }
  given(:grandchild_taxon) { create(:taxon, name: "grandchild taxon", taxonomy: taxonomy, parent: child_taxon) }
  given!(:product1) { create(:product, name: "product1", price: 123.00, taxons: [taxonomy.root]) }
  given!(:product2) { create(:product, name: "product2", price: 456.00, taxons: [child_taxon]) }
  given!(:product3) { create(:product, name: "product3", price: 789.00, taxons: [grandchild_taxon]) }

  background do
    visit potepan_category_path(taxonomy.root.id)
  end

  scenario "親カテゴリーに属する商品が全て表示されている" do
    expect(page).to have_http_status(200)
    expect(current_path).to eq potepan_category_path(taxonomy.root.id)
    expect(page).to have_title("Brand")

    within(".page-title") do
      expect(page).to have_content("Brand")
    end

    within(".productsContent") do
      expect(page).to have_content("product1")
      expect(page).to have_content("$123.00")
      expect(page).to have_content("product2")
      expect(page).to have_content("$456.00")
      expect(page).to have_content("product3")
      expect(page).to have_content("$789.00")
    end
  end

  scenario "子カテゴリーに移動しても属する商品が全て表示されている" do
    within (".sideBar") do
      click_link("child taxon", match: :first)
    end

    expect(page).to have_http_status(200)
    expect(current_path).to eq potepan_category_path(child_taxon.id)
    expect(page).to have_title("child taxon")

    within(".page-title") do
      expect(page).to have_content("child taxon")
    end

    within(".productsContent") do
      expect(page).not_to have_content("product1")
      expect(page).not_to have_content("$123.00")
      expect(page).to have_content("product2")
      expect(page).to have_content("$456.00")
      expect(page).to have_content("product3")
      expect(page).to have_content("$789.00")
    end
  end

  scenario "孫カテゴリーに移動しても属する商品が全て表示されている" do
    within (".sideBar") do
      click_link("grandchild taxon")
    end

    expect(page).to have_http_status(200)
    expect(current_path).to eq potepan_category_path(grandchild_taxon.id)
    expect(page).to have_title("grandchild taxon")

    within(".page-title") do
      expect(page).to have_content("grandchild taxon")
    end

    within(".productsContent") do
      expect(page).not_to have_content("product1")
      expect(page).not_to have_content("$123.00")
      expect(page).not_to have_content("product2")
      expect(page).not_to have_content("$456.00")
      expect(page).to have_content("product3")
      expect(page).to have_content("$789.00")
    end
  end

  scenario "正しく商品詳細ページに飛ぶ" do
    within(".productsContent") do
      expect(page).to have_content("product1")
      expect(page).to have_content("$123.00")

      click_link("product1")
    end

    expect(page).to have_http_status(200)
    expect(current_path).to eq potepan_product_path(product1)
    expect(page).to have_title("product1")

    within(".page-title") do
      expect(page).to have_content("product1")
    end

    within(".singleProduct") do
      expect(page).to have_content("product1")
      expect(page).to have_content(product1.description)
      expect(page).to have_content("$123.00")
    end
  end
end
