# frozen_string_literal: true

require "rails_helper"

feature "#index Potepan::Home" do
  given!(:product1) { create(:base_product, name: "product1", price: 100.00, available_on: 1.month.ago) }
  given!(:product2) { create(:base_product, name: "product2", price: 200.00, available_on: 2.months.ago) }
  given!(:product3) { create(:base_product, name: "product3", price: 300.00, available_on: 3.months.ago) }
  given!(:product4) { create(:base_product, name: "product4", price: 400.00, available_on: 4.months.ago) }
  given!(:product5) { create(:base_product, name: "product5", price: 500.00, available_on: 5.months.ago) }
  given!(:product6) { create(:base_product, name: "product6", price: 600.00, available_on: 6.months.ago) }
  given!(:product7) { create(:base_product, name: "product7", price: 700.00, available_on: 7.months.ago) }
  given!(:product8) { create(:base_product, name: "product8", price: 800.00, available_on: 8.months.ago) }
  given!(:product9) { create(:base_product, name: "product9", price: 900.00, available_on: 9.months.ago) }

  background do
    visit potepan_path
  end

  feature "ヘッダーが正しく表示されている" do
    scenario "各種SNSリンクが表示されている" do
      within ".sns-btn" do
        expect(page).to have_selector ".fa-twitter"
        expect(page).to have_selector ".fa-facebook"
        expect(page).to have_selector ".fa-dribbble"
        expect(page).to have_selector ".fa-vimeo"
        expect(page).to have_selector ".fa-tumblr"
      end
    end

    scenario "アカウント周りのモーダルリンクが表示されている" do
      within ".pull-right" do
        expect(page).to have_link "Log in"
        expect(page).to have_link "Create an account"
      end
    end

    scenario "検索とカートのドロップダウンメニューが表示されている" do
      within ".search-btn" do
        expect(page).to have_selector ".fa-search"
      end

      within ".cart-btn" do
        expect(page).to have_selector ".fa-shopping-cart"
      end
    end

    scenario "サイトマップのドロップダウンメニューが表示されている" do
      within ".navbar-nav" do
        expect(page).to have_link "Home"
        expect(page).to have_link "Shop"
        expect(page).to have_link "Pages"
        expect(page).to have_link "Blog"
        expect(page).to have_link "My Account"
      end
    end
  end

  feature "新着商品が表示されている" do
    scenario "新着商品が取得できる" do
      expect(page).to have_http_status 200
      expect(current_path).to eq potepan_path
      expect(page).to have_title "BIGBAG Store"

      within (".featuredProducts") do
        expect(page).to have_content(product1.name)
        expect(page).to have_content("$100.00")
        expect(page).to have_content(product2.name)
        expect(page).to have_content("$200.00")
        expect(page).to have_content(product3.name)
        expect(page).to have_content("$300.00")
        expect(page).to have_content(product4.name)
        expect(page).to have_content("$400.00")
        expect(page).to have_content(product5.name)
        expect(page).to have_content("$500.00")
        expect(page).to have_content(product6.name)
        expect(page).to have_content("$600.00")
        expect(page).to have_content(product7.name)
        expect(page).to have_content("$700.00")
        expect(page).to have_content(product8.name)
        expect(page).to have_content("$800.00")
        expect(page).not_to have_content(product9.name)
        expect(page).not_to have_content("$900.00")
      end
    end

    scenario "新着商品から正しく商品詳細ページに遷移できる" do
      within(".featuredProducts") do
        click_link(product1.name)
      end

      expect(page).to have_http_status 200
      expect(current_path).to eq potepan_product_path(product1)
      expect(page).to have_title(product1.name)

      within(".singleProduct") do
        expect(page).to have_content(product1.name)
        expect(page).to have_content(product1.description)
        expect(page).to have_content("$100.00")
      end
    end
  end
end
