require "rails_helper"

RSpec.describe Potepan::ProductsController, type: :controller do
  let(:taxonomy1) { create(:taxonomy) }
  let(:taxonomy2) { create(:taxonomy) }
  let(:product) { create(:base_product, taxons: [taxonomy1.root]) }
  let!(:other_product) { create(:base_product, taxons: [taxonomy2.root]) }
  let!(:product_property) { create(:product_property, product: product) }

  describe "GET #show" do
    before { get :show, params: { id_or_slug: product.id } }

    describe "ページ表示について" do
      it "正しく200が返っていること" do
        expect(response.status).to eq 200
      end

      it "正しく:showテンプレートをレンダリングしていること" do
        expect(response).to render_template :show
      end
    end

    describe "idで取得する商品について" do
      it "@productが適切に取得されていること" do
        expect(assigns(:product)).to eq(product)
      end

      it "@product_propertiesが適切に取得されていること" do
        expect(assigns(:product_properties)).to contain_exactly(product_property)
      end
    end

    describe "関連商品について" do
      it "@related_productsに@product自身が含まれていないこと" do
        expect(assigns(:related_products)).not_to include(product)
      end

      it "@related_productsに別taxonの商品が含まれていないこと" do
        expect(assigns(:related_products)).not_to include(other_product)
      end

      context "関連商品が3つの場合" do
        let!(:related_products) { create_list(:base_product, 3, taxons: [taxonomy1.root]) }

        it "@related_productsが3つのみ取得されていること" do
          expect(assigns(:related_products).count).to eq 3
          expect(assigns(:related_products)).to match_array(related_products)
        end
      end

      context "関連商品が4つの場合" do
        let!(:related_products) { create_list(:base_product, 4, taxons: [taxonomy1.root]) }

        it "@related_productsが4つ取得されていること" do
          expect(assigns(:related_products).count).to eq 4
          expect(assigns(:related_products)).to match_array(related_products)
        end
      end

      context "関連商品が4つ以上の場合" do
        let!(:related_products) { create_list(:base_product, 30, taxons: [taxonomy1.root]) }

        it "@related_productsが適切に4つのみ取得されていること" do
          expect(assigns(:related_products).count).to eq 4
        end
      end

      context "関連商品がない場合" do
        it "@related_productsが1つも取得されていないこと" do
          expect(assigns(:related_products).count).to eq 0
        end
      end
    end
  end
end
