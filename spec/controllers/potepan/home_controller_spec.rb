require "rails_helper"

RSpec.describe Potepan::HomeController, type: :controller do
  describe "GET #index" do
    before { get :index }

    describe "ページ表示について" do
      it "ステータスコードは200" do
        expect(response.status).to eq 200
      end

      it "indexテンプレートを表示できる" do
        expect(response).to render_template :index
      end
    end

    describe "新着商品について" do
      let!(:new_products) do
        [
          create(:base_product, available_on: 1.month.ago),
          create(:base_product, available_on: 2.months.ago),
          create(:base_product, available_on: 3.months.ago),
          create(:base_product, available_on: 4.months.ago),
          create(:base_product, available_on: 5.months.ago),
          create(:base_product, available_on: 6.months.ago),
          create(:base_product, available_on: 7.months.ago),
          create(:base_product, available_on: 8.months.ago)
        ]
      end
      let!(:product9) { create(:base_product, available_on: 9.months.ago) }

      it "新着商品を取得できる" do
        expect(assigns(:new_products)).to match_array(new_products)
      end

      it "取得する数は8つ" do
        expect(assigns(:new_products).count).to eq 8
      end

      it "新着順が9番目の商品は取得しない" do
        expect(assigns(:new_products)).not_to include(product9)
      end
    end
  end
end
