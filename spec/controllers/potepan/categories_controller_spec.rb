require "rails_helper"

RSpec.describe Potepan::CategoriesController, type: :controller do
  let(:taxonomy) { create(:taxonomy) }
  let(:child_taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
  let(:grandchild_taxon) { create(:taxon, taxonomy: taxonomy, parent: child_taxon) }
  let!(:product1) { create(:product, taxons: [taxonomy.root]) }
  let!(:product2) { create(:product, taxons: [child_taxon]) }
  let!(:product3) { create(:product, taxons: [grandchild_taxon]) }

  describe "GET #show" do
    before do
      get :show, params: { id: child_taxon.id }
    end

    it "正しく200が返っていること" do
      expect(response.status).to eq 200
    end

    it "正しく:showテンプレートを表示していること" do
      expect(response).to render_template :show
    end

    it "@productsが適切に取得されていること" do
      expect(assigns(:products)).to contain_exactly(product2, product3)
    end

    it "@taxonが適切に取得されていること" do
      expect(assigns(:taxon)).to eq(child_taxon)
    end

    it "@taxonomiesが適切に取得されていること" do
      expect(assigns(:taxonomies)).to contain_exactly(taxonomy)
    end
  end
end
