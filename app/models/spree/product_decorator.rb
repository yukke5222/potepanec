Spree::Product.class_eval do
  scope :include_images_and_prices, -> {
    includes(master: %i(default_price images))
  }

  scope :related_products, ->(product) {
    joins(:taxons).
      where(spree_taxons: { id: product.taxons.ids }).
      where.not(id: product.id).
      distinct
  }

  scope :new_products, -> {
    order(available_on: "DESC")
  }

  scope :random_products, -> {
    where(id: self.pluck(:id))
  }
end
