class Potepan::ProductsController < ApplicationController
  RELATED_PRODUCTS_COUNT = 4

  def show
    @product = Spree::Product.friendly.find(params[:id_or_slug])
    @product_properties = @product.product_properties.includes(:property)
    @related_products = Spree::Product.
                          include_images_and_prices.
                            related_products(@product).
                            limit(RELATED_PRODUCTS_COUNT)
  end
end
