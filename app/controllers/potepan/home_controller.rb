class Potepan::HomeController < ApplicationController
  NEW_PRODUCTS_COUNT = 8
  RANDOM_PRODUCT_COUNT = 3

  def index
    @new_products = Spree::Product.
                      include_images_and_prices.
                      new_products.
                      limit(NEW_PRODUCTS_COUNT)
    @random_products = Spree::Product.
                         include_images_and_prices.
                         random_products.
                         limit(RANDOM_PRODUCT_COUNT)
  end
end
